#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Version 1.0 Beta, May 2021, Fabio Oriani, University of Lausanne
"""
#%% LOAD PACKAGES
import warnings
warnings.filterwarnings("ignore")
from PIL import Image
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'family' : 'DejaVu Sans',
        'size'   : 14}
rc('font', **font)
from scipy.signal import fftconvolve
#from matplotlib_scalebar.scalebar import ScaleBar
from dtaidistance import dtw
from scipy import signal
#from skimage.morphology import disk
from tkinter import Tk     # from tkinter import Tk for Python 3.x
from tkinter.filedialog import askopenfilename
from tkinter import messagebox
#from matplotlib.widgets import Button
from matplotlib.widgets import Slider

#%% IMPORT DATA
## command line argument
#im=np.array(Image.open(str(sys.argv[1])))[:,:1000];
#sname=str(sys.argv[1]);

print(
    """
    WLCOUNT  Copyright (C) 2021 Fabio Oriani, University of Lausanne
    This program comes with ABSOLUTELY NO WARRANTY under GNU General public 
    Licence v.3 or later. For usage information see:
        https://bitbucket.org/orianif/wlcount/
    """
    )

# file choice dialog
root=Tk() # open dialog
filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file

im=np.array(Image.open(filename));
if np.ndim(im)==3:
    im=np.mean(im,axis=2)
sname=str(filename);

# Look for previous alignment files
filepres='yes'
try:
    np.load(sname + '.npy')
    np.load(sname + '_pic.npy')
    np.load(sname + '_im.npy')
except IOError:
    print("No complete suite of previous alignment files found --> proceeding with the alignment")
    filepres='no'

# Ask previous alignment files
if filepres=='yes':
    ans=messagebox.askquestion("Form",
                               "Previous alignment files found: do you want to use them to speed up the computation?")
        
else:
    ans='no'
root.withdraw() # close dialog

#%% DEFAULT PREPROCESSING
if ans=="no":
# ask for applcation of the moving average
    root=Tk() # open dialog
    ans2=messagebox.askquestion("Form",
                               "Do you want to apply the moving-average preprocessing? YES (suggested) to remove small-scale noise, NO to detect laminae thinner than 5 pixels.")
    root.withdraw() # close dialog
## moving average
    if ans2=='yes':
        w=5
        k=np.array([[0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
               [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
               [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
               [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
               [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
               [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
               [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
               [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
               [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
               [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
               [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]], dtype="uint8")
        im=fftconvolve(im,k/np.sum(k),mode="valid")
        im=np.pad(im,int(w),mode='edge') # pad to obtain the same size as ti2

    #% ALIGN E SUM - NESTED STRUCTURE
    im_in=im.astype("double")
    
    # take out last line of the image if odd
    if np.mod(np.shape(im_in)[0],2)>0:
        im_in=im_in[:-1,:]
    m=0
    print("%%%% EXTRACTING TIME SERIES %%%%")
    while np.shape(im_in)[0]>1:
        # define output image
        ix=int(np.shape(im_in)[0]/2)
        jx=np.shape(im_in)[1]
        im_out=np.empty((ix,jx))*np.nan
        
        # align
        m=m+1 # alignment counter
        prog_old=0 # internal progress counter
        n=0
        print("%%%% level n." + str(m) + " %%%%")
        for i in range(0,np.shape(im_in)[0]-1,2):
            a=np.copy(im_in[i,:]) # 1st lines
            b=np.copy(im_in[i+1,:]) # next line
            b_t=dtw.warp(b,a,window=10)[0]
            b_t=np.mean(np.vstack((a,b_t)),axis=0) # sum to a
            im_out[n,:]=b_t # put result into new image
            n=n+1
            # progress counter
            prog=round((n/(np.shape(im_out)[0]))*100)
            if prog!=prog_old:
                sys.stdout.write('\r'+str(prog)+ "% completed")
                prog_old=prog
        print(", output size = ",ix,jx)
        
        # update im_in
        im_in=np.copy(im_out)
        
    ts=np.squeeze(im_out)
    # save time series
    np.save(sname,ts)
    
    #% ALLING IMAGE TO OBTAINED TIME-SERIES
    im_in=im.astype("double")
    im_out=im.astype("double")
#    ts=np.load(sname + '.npy')
    a=np.copy(ts)
    print("%%%% IMAGE ALIGNMENT %%%%")
    for i in range(0,np.shape(im_in)[0]):
        b=np.copy(im_in[i,:]) # 1st lines
        b_t=dtw.warp(b,a,window=10)[0]
        im_out[i,:]=np.copy(b_t) # put result into new image
        # progress counter
        prog=round((i/(np.shape(im_out)[0]))*100)
        sys.stdout.write('\r'+str(prog)+ "% completed"+"\t")
    
    np.save(sname + "_pic",im_out)
    np.save(sname + "_im",im)
    print("%%%% ALIGNMENT COMPLETED %%%%")
else:
    print("%%%% LOADING PRECOMPUTED ALIGNMENT %%%%")
    im=np.load(sname + '_im.npy')
    ts=np.load(sname + '.npy')
    im_out=np.load(sname + '_pic.npy')

#%% IMAGE + TIME SERIES
### colorscale limits for the image

s=np.std(im)
m=np.mean(im)
lb=m-2*s
ub=m+2*s

x=np.arange(0,len(ts))    
w=int(len(ts)/100)
fig=plt.figure(figsize=[30,  10 ])
sp0=plt.subplot(311)
delta=(np.nanmax(ts)-np.nanmin(ts))/10
im1=plt.imshow(im,extent=[0,len(x),np.nanmin(ts)-delta,np.nanmax(ts)+delta],vmin=lb,vmax=ub)
plt.xticks([], [])
plt.yticks([], [])

plt.title(sname + ' original section')
plt.axis("auto")
plt.tight_layout()
sp=plt.subplot(312,sharex=sp0)
delta=(np.nanmax(ts)-np.nanmin(ts))/10
im2=plt.imshow(im_out,extent=[0,len(x),np.nanmin(ts)-delta,np.nanmax(ts)+delta],vmin=lb,vmax=ub)
plt.xticks([], [])
plt.yticks([], [])
x=np.arange(0,len(ts))
plt.plot(ts,c="red",label="Extracted time series " )
plt.title('Aligned section and obtained band time series')
plt.axis("auto")
plt.tight_layout()
#plt.legend(loc="upper left")
plt.legend()

#% wavelet transform
widths = np.arange(1, np.shape(ts)[-1]/5)
cwtmatr = signal.cwt(ts, signal.ricker, widths)

wlb=np.quantile(cwtmatr,0.1)/10
wub=np.quantile(cwtmatr,0.9)/10
ax = plt.subplot(313,sharex=sp0)#(212)

h=plt.imshow(cwtmatr, extent=[1, np.shape(ts)[-1], widths[-1], 1], cmap='PRGn', aspect='auto',
          vmax=-wlb, vmin=wlb) # symmetric color limits

plt.yscale("log")
plt.ylim([widths[-1]+1,0.9])
plt.ylabel("wavelet scale [pixels]")
plt.xlabel("section x axis [pixels]")
plt.title("Wavelet transform of the time series")
plt.tight_layout()
sp.set_xlim([0,len(x)])
sp.set_ylim([np.nanmin(ts)-delta,np.nanmax(ts)+delta])

# Make a vertically oriented slider to control the amplitude
axcolor = 'lightgoldenrodyellow'
axamp = plt.axes([0.01, 0.5, 0.00625, 0.43], facecolor=axcolor)
amp_slider = Slider(
    ax=axamp,
    label="Contrast",
    valmin=-10,
    valmax=+2,
    valinit=0,
    orientation="vertical"
)

# The function to be called anytime a slider's value changes
def update(val):
    im1.set_clim(vmin=lb+s*amp_slider.val,vmax=ub-s*amp_slider.val)
    im2.set_clim(vmin=lb+s*amp_slider.val,vmax=ub-s*amp_slider.val)
amp_slider.on_changed(update)

#%% DRAW WL SECTIONS
print("performed band counts: ")
prop_cycle = plt.rcParams['axes.prop_cycle'] # get deafult line colors
colors = prop_cycle.by_key()['color']
k=[0] # iterator for colors

# these lists are affected by callbacks below
secx=[] # section coordinates
secy=[]
px=[] # point coordinates
py=[]
p=[] # pointers to point plot
#ev=[None] # last event type
segp=[] # pointers to segment plot
cr=[] # cross simbols for counts
crw=[] # white cross simbols for counts
cts_out=[] # counted points time series
count_out=[] # counts
cts_out3=[] # output count time series
#cts_out3.append(x[:,None])
cts_tit=[] # heading for out file
cts_tit.append('band_number')

#ev=[0]



#def wait_until(somepredicate, timeout, period=0.25):
#  mustend = time.time() + timeout
#  while time.time() < mustend:
#    if somepredicate: return True
#    time.sleep(period)
#  return False


def on_button(event): # SEGMENT DRAWING
    segx=[]
    segy=[]
    #print('you pressed', event.button, event.xdata, event.ydata)
    #print(event.inaxes)
    #ev[0]=event.inaxes
    
#    if event.inaxes!=ax: # error if point is outside graph
#        root=Tk()
#        messagebox.showerror('error', 'Draw points inside the wavelet transform array (bottom graph)')
#        root.withdraw()
        
    if event.button==1 and px==[] and event.inaxes==ax: # add starting point
        px.append(np.copy(event.xdata))
        py.append(np.copy(event.ydata))
        p.append(ax.scatter(px[-1].round(),py[-1].round(),marker="x",c=colors[k[0]]))
        plt.draw()   
            
    elif  event.button==3 and len(px)==1 and event.inaxes==ax: # delete starting point
        p[-1].remove(); plt.draw() # remove wrong point 
        del p[-1]
        del px[-1]
        del py[-1]
        
    elif event.button==1 and len(px)>0 and event.inaxes==ax: # add ending point and make segment
        
        if event.xdata<=px[-1]: # error if point is not on the right of last point
             root=Tk()
             messagebox.showerror('error', 'Draw the section points from left to right')
             root.withdraw()
             
        else:
            px.append(np.copy(event.xdata))
            py.append(np.copy(event.ydata))
            segx=np.arange(px[-2],px[-1]).round() # segment x coordinate             
            p.append(ax.scatter(px[-1].round(),py[-1].round(),marker="x",c=colors[k[0]])) # plot point
            segy=np.round(np.linspace(py[-2],py[-1],num=np.shape(segx)[0])) # segment y coordinate
            segp.append(ax.plot(segx[[0,-1]],segy[[0,-1]],'--',c=colors[k[0]])[0]) # plot the segment
            plt.draw()
            secx.append(segx) # add segment to the section
            secy.append(segy)
            # DRAW BAND COUNT MARKS
            ts_wl=cwtmatr[np.array(segy).astype('int'),np.array(segx).astype('int')] # obtain time-series from wavelet
        #   ts_wl=ts_wl/np.std(ts_wl)*np.std(ts)/2
            # band count
            #ev.append(ts_wl)
            x=np.copy(segx)
            # average of the time-series for threshold
            ts_ma=np.array([np.nanmean(ts_wl)]*np.shape(ts_wl)[0])        
#            if np.size(ts_ma)==0: # stop count if obtained times series is empty
#                break   
            sw=0 # band switch
            nb_tmp=0 # band counter
            cts=np.empty_like(x)*np.nan # count time series
        #    cts_w=np.copy(cts)
            ctsy=np.nanmin(ts)+(np.nanmax(ts)-np.nanmin(ts))/10*k[0]# y value for cts
            for j in range(len(ts_wl)):
                if ts_wl[j]>ts_ma[j] and sw==0:
                    sw=1
                    b_st=j # band start
                    nb_tmp=nb_tmp+1
                elif ts_wl[j]<ts_ma[j] and sw==1:
                    sw=0
                    cts[round(b_st+(j-b_st)/2)]=ctsy # time series to display
            crw.append(sp.scatter(x,cts,marker="x",c="white",s=90))
            cr.append(sp.scatter(x,cts,marker="x",s=40,c=colors[k[0]]))            
            plt.draw()
            cts_out.append(np.copy(cts)) # update count time series


    elif  event.button==3 and len(px)>1 and event.inaxes==ax: # delete last point and segment
        p[-1].remove()
        segp[-1].remove()
        cr[-1].remove()
        crw[-1].remove()
        plt.draw()
        del p[-1]
        del px[-1]
        del py[-1]
        del segp[-1]
        del secx[-1]
        del secy[-1]
        del cr[-1]
        del crw[-1]              
    #print(secx)

def on_key(event): # KEYBOARD BUTTONS TO RESET OR FINISH A SINGLE COUNT
#    print('you pressed', event.key, event.xdata, event.ydata)
    if event.key=='enter' and len(cr)>0: # finish a count
        secx_out=np.concatenate(secx).astype('int') # concatenate segments
        cts_out2=np.concatenate(cts_out)
        secx_out, ind = np.unique(secx_out, return_index=True); # remove double x points
        cts_out2=cts_out2[ind.astype('int')]
#        cts_out2=np.concatenate((np.zeros(secx_out[0]),cts_out2,np.zeros(len(ts)-secx_out[-1]-1)))
#        cts_out2[np.isnan(cts_out2)]=0
#        count_tmp=np.sum(cts_out2!=0)
        secx_out=secx_out[np.isfinite(cts_out2)]
        count_tmp=len(secx_out)
        cts_out3.append(secx_out[:,None])
        cts_tit.append('count' + str(k[0]) + ': ' + str(count_tmp)) # add column to ouput txt        
        print('count' + str(k[0]) + ': ' + str(count_tmp)) #print count
        cr[0].set_label('count ' + str(k[0]) + ': ' + str(count_tmp) + ' bands')
        sp.legend(),plt.draw() # update legend
        count_out.append(count_tmp)
        secx.clear() # clear lists for next count
        secy.clear()
        px.clear()
        py.clear()
        p.clear()
        segp.clear()
        cr.clear()
        crw.clear()
        cts_out.clear()
        k[0]=k[0]+1 # update count counter
        
    elif event.key=='ctrl+r': # reset all counts
        for point in p: point.remove() # remove points and segments
        for seg in segp: seg.remove()
        for c in cr: c.remove()
        for c in crw: c.remove()
        plt.draw()
        p.clear()
        px.clear()
        py.clear()
        segp.clear()
        secx.clear()
        secy.clear()
        cr.clear()
        crw.clear()
        
#    elif event.key=='ctrl+q': # abort program
#        fig.canvas.mpl_disconnect(cid)
#        fig.canvas.mpl_disconnect(cid2)
#        fig.canvas.mpl_disconnect(cid3)
#        print("%%%% COUNTS WITHRAWN BY THE USER %%%%")
#        sys.exit()
        
    elif event.key=='ctrl+e': # IMAGE CLOSING TO FINISH ALL COUNTS
        plt.savefig(sname + ".pdf",format="pdf") # save figure
        cts_tit2=np.array(cts_tit)
        l=[] # find max time series length
        for i in range(len(cts_out3)):
            l.append(len(cts_out3[i]))
        max_l=max(l)
        for i in range(len(cts_out3)):
            delta_fill=-np.ones(max_l-len(cts_out3[i]))
            cts_out3[i]=np.concatenate((cts_out3[i],delta_fill[:,None]))
        cts_out4=np.concatenate(cts_out3,axis=1)
        cts_out4=np.concatenate((np.arange(max_l)[:,None],cts_out4),axis=1)
        
        # save count time series
        f = open(sname + "_counts.dat", "w")
        np.savetxt(f,cts_tit2[None,:], delimiter=",",fmt="%s")
        np.savetxt(f,cts_out4,delimiter=",",fmt="%d")
        f.close()
        with open(sname + "_counts.dat", 'r') as fi:
            #read file
            file_source = fi.read()
        with open(sname + "_counts.dat", 'w') as fo:
            replace_string = file_source.replace('-1', '')
            #save output
            fo.write(replace_string)
        fi.close()
        fo.close()
        print("%%%% COUNT SUCCESSFULLY ENDED - OUTPUT FILES WRITTEN IN THE IMAGE FOLDER %%%%")
    #    ev[0]=1
        fig.canvas.mpl_disconnect(cid)
        fig.canvas.mpl_disconnect(cid2)
        fig.canvas.mpl_disconnect(cid3)
        sys.exit()

def on_close(event): # exit without saving the counts
        print("%%%% COUNT CANCELLED BY THE USER %%%%")
        fig.canvas.mpl_disconnect(cid)
        fig.canvas.mpl_disconnect(cid2)
        fig.canvas.mpl_disconnect(cid3)
        sys.exit()

#sys.exit()
# CONNECT EVENTS
if 'cid' in locals():
    fig.canvas.mpl_disconnect(cid) # disconnet any previous cid
cid = fig.canvas.mpl_connect('button_press_event', on_button) # connect button event
if 'cid2' in locals():
    fig.canvas.mpl_disconnect(cid2) # disconnet any previous cid
cid2 = fig.canvas.mpl_connect('key_press_event', on_key) # connect keyboard event
if 'cid3' in locals():
    fig.canvas.mpl_disconnect(cid3) # disconnet any previous cid
cid3= fig.canvas.mpl_connect('close_event', on_close)  # connect close window event

plt.show()
plt.sca(ax)
